package repository;

import model.Model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Repository {
    protected EntityManager em;

    public Repository() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("main");
        this.em = emf.createEntityManager();
    }

    public void save(Model m) {
        EntityTransaction userTransaction = em.getTransaction();
        userTransaction.begin();
        em.persist(m);
        em.flush();
        userTransaction.commit();
    }

    public void delete(Model m) {
        EntityTransaction userTransaction = em.getTransaction();
        userTransaction.begin();
        em.remove(m);
        em.flush();
        userTransaction.commit();
    }
}
