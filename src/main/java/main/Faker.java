package main;

import model.Agent;
import model.Tool;
import model.Visitor;
import repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class Faker {
    private Faker() {
        throw new IllegalStateException("Utility class");
    }

    public static void generate() {
        Repository repository = new Repository();

        // Agent
        Agent a = new Agent(
                "john.doe@gmail.com",
                "John",
                "Doe",
                "password"
        );
        repository.save(a);

        // Visitor
        Visitor v = new Visitor(
                "jane.doe@gmail.com",
                "Jane",
                "Doe",
                "password"
        );
        repository.save(v);

        // Tools
        Integer qty = 2;
        List<Tool> tools = new ArrayList<>();
        tools.add(new Tool("Mousqueton", qty));
        tools.add(new Tool("Gants d'intervention", qty));
        tools.add(new Tool("Brassard de sécurité", qty));
        tools.add(new Tool("Porte menottes", qty));
        tools.add(new Tool("Bandeau agent de sécurité cynophile", qty));
        tools.add(new Tool("Talkie walkie", qty));
        tools.add(new Tool("Lampe Torche", qty));
        tools.add(new Tool("Kit oreillette", qty));
        tools.add(new Tool("Taser", qty));
        tools.add(new Tool("Bombe lacrymogène", qty));

        a.rent(tools.get(0));

        for (Tool t : tools) {
            repository.save(t);
        }
    }
}
