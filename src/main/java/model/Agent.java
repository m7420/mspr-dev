package model;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
public class Agent extends User {
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "rental",
            joinColumns = @JoinColumn(name = "agent_id"),
            inverseJoinColumns = @JoinColumn(name = "tool_id"))
    private final Set<Tool> tools = new LinkedHashSet<>();

    public Agent() {
    }

    public Agent(String email, String firstName, String lastName, String password) {
        super(email, firstName, lastName, password);
    }

    public Set<Tool> getTools() {
        return tools;
    }

    public void rent(Tool t) {
        if (0 == t.getQuantity()) {
            throw new IllegalArgumentException("Tool out of stock");
        }

        tools.add(t);
    }
}