package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToolTest {
    @Test
    void it_must_create_an_empty_tool() {
        Tool t = new Tool();

        assertNull(t.getName());
        assertNull(t.getQuantity());
        assertEquals(0, t.getAgents().size());
    }

    @Test
    void it_must_create_a_tool() {
        String name = "Mousqueton";
        Integer qty = 2;

        Tool t = new Tool(name, qty);

        assertNull(t.getId());
        assertEquals(name, t.getName());
        assertEquals(qty, t.getQuantity());
        assertEquals(0, t.getAgents().size());
    }
}