package model;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class VisitorTest {
    @Test
    void it_must_create_an_empty_visitor() {
        Visitor v = new Visitor();

        assertNull(v.getId());
        assertNull(v.getEmail());
        assertNull(v.getFirstName());
        assertNull(v.getLastName());
        assertNull(v.getPassword());
        assertNull(v.getRegisteredAt());
    }

    @Test
    void it_must_create_visitor() {
        String email = "jane.doe@gmail.com";
        String firstName = "Jane";
        String lastName = "Doe";
        String password = "password";

        Visitor v = new Visitor(email, firstName, lastName, password);

        assertNull(v.getId());
        assertEquals(email, v.getEmail());
        assertEquals(firstName, v.getFirstName());
        assertEquals(lastName, v.getLastName());
        assertEquals(password, v.getPassword());
        assertInstanceOf(Date.class, v.getRegisteredAt());
    }
}