package repository;

import exception.AgentNotFoundException;
import exception.MultipleAgentsFoundException;
import model.Agent;

import javax.persistence.TypedQuery;
import java.util.List;

public class AgentRepository extends Repository {

    public Agent findOneByEmailAndPassword(String email, String password) throws AgentNotFoundException, MultipleAgentsFoundException {
        TypedQuery<Agent> query = em.createQuery(
                "SELECT a FROM Agent a WHERE a.email = :email AND a.password = :password",
                Agent.class
        );
        query.setParameter("email", email);
        query.setParameter("password", password);

        List<Agent> results = query.getResultList();

        if (results.isEmpty()) {
            throw new AgentNotFoundException();
        }

        if (results.size() > 1) {
            throw new MultipleAgentsFoundException(results);
        }

        return results.get(0);
    }

}
