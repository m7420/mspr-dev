package exception;

import model.Agent;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultipleAgentsFoundExceptionTest {
    @Test
    void it_must_create_multiple_agents_found_exception() {
        Agent a = new Agent(
                "john.doe@gmail.com",
                "John",
                "Doe",
                "password"
        );
        List<Agent> agentList = new ArrayList<>();
        agentList.add(a);

        try {
            throw new MultipleAgentsFoundException(agentList);
        } catch (MultipleAgentsFoundException e) {
            assertEquals("Multiple agents found", e.getMessage());
            assertEquals(agentList, e.getAgents());
        }
    }
}
