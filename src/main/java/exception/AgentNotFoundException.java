package exception;

public class AgentNotFoundException extends Exception {
    public AgentNotFoundException() {
        super("Agent not found");
    }
}
