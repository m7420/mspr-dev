package model;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class AgentTest {
    @Test
    void it_must_create_an_empty_agent() {
        Agent a = new Agent();

        assertNull(a.getId());
        assertNull(a.getEmail());
        assertNull(a.getFirstName());
        assertNull(a.getLastName());
        assertNull(a.getPassword());
        assertNull(a.getRegisteredAt());
        assertEquals(0, a.getTools().size());
    }

    @Test
    void it_must_create_an_agent() {
        String email = "john.doe@gmail.com";
        String firstName = "John";
        String lastName = "Doe";
        String password = "password";

        Agent a = new Agent(email, firstName, lastName, password);

        assertNull(a.getId());
        assertEquals(email, a.getEmail());
        assertEquals(firstName, a.getFirstName());
        assertEquals(lastName, a.getLastName());
        assertEquals(password, a.getPassword());
        assertInstanceOf(Date.class, a.getRegisteredAt());
        assertEquals(0, a.getTools().size());
    }

    @Test
    void it_must_rent() {
        Agent a = new Agent();
        Tool t = new Tool("Mousqueton", 2);

        a.rent(t);

        assertEquals(1, a.getTools().size());
    }

    @Test
    void it_must_not_rent_and_throw_an_exception() {
        Agent a = new Agent();
        Tool t = new Tool("Mousqueton", 0);

        assertThrows(IllegalArgumentException.class, () -> {
            a.rent(t);
        });
    }
}
