package exception;

import model.Agent;

import java.io.Serializable;
import java.util.List;

public class MultipleAgentsFoundException extends Exception implements Serializable {
    private final List<Agent> agents;

    public MultipleAgentsFoundException(List<Agent> agentList) {
        super("Multiple agents found");
        agents = agentList;
    }

    public List<Agent> getAgents() {
        return agents;
    }
}
