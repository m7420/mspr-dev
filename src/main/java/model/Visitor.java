package model;

import javax.persistence.Entity;

@Entity
public class Visitor extends User implements Model {
    public Visitor() {
    }

    public Visitor(String email, String firstName, String lastName, String password) {
        super(email, firstName, lastName, password);
    }
}