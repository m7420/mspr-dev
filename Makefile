SONAR	= /mnt/c/Program\ Files/Sonarqube/bin/linux-x86-64/sonar.sh
MAVEN	= mvn

mysql:
	sudo service mysql start

install:
	@$(MAVEN) clean install -f pom.xml

verify:
	@$(MAVEN) clean verify

sonar:
	@$(MAVEN) jacoco:report sonar:sonar -f pom.xml \
	-Dsonar.host.url=http://localhost:9000 \
	-Dsonar.login=$(SONAR_LOGIN) \
	-Dsonar.projectKey=m7420_mspr-dev_AX6m9l72pfhl6yVm7tJX

sonar-start:
	@$(SONAR) start

sonar-stop:
	@$(SONAR) stop

