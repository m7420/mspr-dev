package exception;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AgentNotFoundExceptionTest {
    @Test
    void it_must_create_agent_not_found_exception() {
        try {
            throw new AgentNotFoundException();
        } catch (AgentNotFoundException e) {
            assertEquals("Agent not found", e.getMessage());
        }
    }
}
